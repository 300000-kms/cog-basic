import './style.css';
//import 'ol/ol.css';
import 'ol-layerswitcher/dist/ol-layerswitcher.css';

import {Map, View} from 'ol';
import {WebGLTile as WebGLTileLayer, Tile as TileLayer} from 'ol/layer';
import {GeoTIFF, OSM} from 'ol/source';
import {fromLonLat, toLonLat} from 'ol/proj';
import {ScaleLine, defaults as defaultControls, Attribution} from 'ol/control';

import LayerSwitcher from 'ol-layerswitcher';

/*
 * COG
 ******************************/
const cogSource = new GeoTIFF({
  sources: [
    {
      url: './out_1975.tif',
      max: 100,
      crossOrigin: 'anonymous',
    }, 
  ],
});

const cogLayer = new WebGLTileLayer({
  title: 'poblacion 1975',
  source: cogSource,
  interpolate: false,
  //normalize: false,
});

let res0 = 9783.939620502551;

let viewObj = {
  "resolutions": [
    res0,
    res0/2,
    res0/4,
    res0/8,
    res0/16,
    res0/32,
    res0/64,
    res0/128,
    res0/264,
    res0/564,
    res0/1024, // zoom 10
    res0/2048,
  ],
  "center": [
    -704443.6526761837,
    4500612.225431179
  ],
  "extent": [
    -1408887.3053523675,
    3835304.331237005,
    0,
    5165920.119625352
  ],
  "zoom": 1,
};

const map = new Map({
  target: 'map',
  layers: [
    new TileLayer({
      title: "OSM",
      type: "base",
      source: new OSM()
    }),
    
    cogLayer,

    new WebGLTileLayer({
      title: 'poblacion 1990',
      visible: false,
      source: new GeoTIFF({
        sources: [
          {
            url: './out_1990.tif',
            max: 100,
            crossOrigin: 'anonymous',
          }, 
        ],
      })
    })
  ],
  //view: cogSource.getView(),
  view: new View(viewObj),
  controls: defaultControls().extend([new ScaleLine()]),
});

let layerSwitcher = new LayerSwitcher({
  startActive: true,
  activationMode: 'click',
});
map.addControl(layerSwitcher);

let first = true;
map.on('rendercomplete', function() {
  if (first) {
    console.log(map.getView().getProperties());
    first = false;
  }
  else {
    console.log(map.getView().getZoom());
  }
})

map.on('click', function(evt) {
  console.log(cogLayer.getData(evt.pixel));
});
